Feature: Token Business Logic Features

  Scenario: Generate Token
    Given customer "a" with cpr "290196-0013"
    And the customer is in the database
    When "a" requests 1 token
    Then "a" has a token count of 1
	
  Scenario: Generate Too Many Tokens
    Given customer "a" with cpr "290196-0013"
    And the customer is in the database
    When "a" requests 6 token
    Then "a" has a token count of 0

  Scenario: Generate Too Few Tokens
    Given customer "a" with cpr "290196-0013"
    And the customer is in the database
    When "a" requests 0 token
    Then "a" has a token count of 0

  Scenario: fake token return CPR
      Given customer "a" with cpr "290196-0013"
      And the customer is in the database
      And a fake token
 			When the CPR number retrieval is used on the token
 			Then a CPR number is not returned 	
      
 	Scenario: real Token return CPR
 			Given customer "a" with cpr "290196-0013"
 			And the customer is in the database
 			And a token
 			When the CPR number retrieval is used on the token
 			Then a CPR number is returned
 
 	Scenario: has 1 tokens requests 3
 			Given customer "a" with cpr "290196-0013"
 			And the customer is in the database
 			And a token
 			When "a" requests 3 token
 			Then "a" has a token count of 4
 			
 	Scenario: has 2 tokens requests 1
 			Given customer "a" with cpr "290196-0013"
 			And the customer is in the database
 			And a token
 			And a token
 			When "a" requests 3 token
 			Then "a" has a token count of 2

 			
 	Scenario: User does not exist in datbase
 		Given customer "a" with cpr "290196-0073"
 		And the customer does not exist in the datbase
 		When "a" requests 2 token
 		Then the account is added to the database
 		 	