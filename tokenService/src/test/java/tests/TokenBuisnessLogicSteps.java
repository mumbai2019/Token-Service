package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Optional;

import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import customer.UserID;
import customer.UserIDImplementation;
import customer.User;
import customer.UserImplementation;
import logic.TokenHandling;
import token.Token;
import token.TokenID;
import token.TokenImplementation;

/**
 * @author s164166 Patrick
 */
public class TokenBuisnessLogicSteps
{
	User customer;
	TokenHandling handler;
	Token token;
	Optional<UserID> optionalCPR;
	boolean result;
	
	/**
	 * @author s164166 Patrick
	 */
	@Before
	public void init()
	{
		Field field = null;
		try {
			field = TokenHandling.class.getDeclaredField("instance");
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		field.setAccessible(true);
		try {
			field.set(field, null);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		handler = TokenHandling.getInstance();
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Given("^customer \"([^\"]*)\" with cpr \"([^\"]*)\"$")
	public void customerWithCpr(String arg1, String arg2) throws Throwable {
	    customer = new UserImplementation(new UserIDImplementation(arg2));
	}

	/**
	 * @author s164166 Patrick
	 */
	@When("^\"([^\"]*)\" requests (\\d+) token$")
	public void requestsToken(String arg1, int arg2) throws Throwable {
		handler.addTokens(customer.getUserID(), arg2);
	}

	/**
	 * @author s164166 Patrick
	 */
	@Then("^\"([^\"]*)\" has a token count of (\\d+)$")
	public void hasATokenCountOf(String arg1, int arg2) throws Throwable {
	    assertEquals(arg2, handler.getUnusedTokenAmm(customer.getUserID()));
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Given("^a fake token$")
	public void aFakeToken() throws Throwable {
	    token = new TokenImplementation("fake", "fake");
	}

	/**
	 * @author s164166 Patrick
	 */
	@When("^the CPR number retrieval is used on the token$")
	public void theCPRNumberRetrievalIsUsedOnTheToken() throws Throwable {
		optionalCPR = handler.getUserID(token.getID());
	}

	/**
	 * @author s164166 Patrick
	 */
	@Then("^a CPR number is not returned$")
	public void aCPRNumberIsNotReturned() throws Throwable {
	    assertFalse(optionalCPR.isPresent());
	}

	/**
	 * @author s164166 Patrick
	 */
	@Given("^a token$")
	public void aToken() throws Throwable {
	    List<TokenID> tokenIDList = handler.addTokens(customer.getUserID(), 1);
	    if( tokenIDList.size() == 0 )
	    	throw new Exception("No tokens returned");
	    
    	token = handler.getToken(tokenIDList.get(0));
	}

	/**
	 * @author s164166 Patrick
	 */
	@Then("^a CPR number is returned$")
	public void aCPRNumberIsReturned() throws Throwable {
	    assertTrue(optionalCPR.isPresent());
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Given("^the customer does not exist in the datbase$")
	public void theCustomerDoesNotExistInTheDatbase() throws Throwable {
	    assertFalse(handler.userExists(customer.getUserID()));
	}

	/**
	 * @author s164166 Patrick
	 */
	@Then("^the account is added to the database$")
	public void theAccountIsAddedToTheDatabase() throws Throwable {
		assertTrue(handler.userExists(customer.getUserID()));
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Given("^the customer is in the database$")
	public void the_customer_is_in_the_database() throws Throwable {
	    handler.addToDatabase(customer.getUserID());
	}


}