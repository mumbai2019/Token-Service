package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.URI;
import java.util.ArrayList;

import org.apache.tools.ant.taskdefs.Input;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import customer.UserID;
import customer.UserIDImplementation;
import token.Token;
import token.TokenID;
import token.TokenIDImplementation;
import token.TokenRepresentationImplementation;

/**
 * Made to testing TokenIDImplementation.
 * @author s164166 Patrick
 */
public class UserIDImplementationTest
{
	UserID number;
	String id = "229709-1389";
	@Rule public MockitoRule mockitoRule = MockitoJUnit.rule();
	
	/**
	 * @author s164166 Patrick
	 */
	@Before
	public void init()
	{
		number = new UserIDImplementation(id);
	}
	 
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void cPRNumberGetHashcodeIDisNull()
	{
		int result = new UserIDImplementation(null).hashCode();
		assertEquals(291, result);
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void cPRNumbergetHashcode()
	{
		int expected = 291 + id.hashCode();
		int result = number.hashCode();
		assertEquals(expected, result);
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void cPRNumberEqualsSameObject()
	{
		boolean result = number.equals(number);
		assertTrue(result);
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void cPRNumberEqualsNull()
	{
		boolean result = number.equals(null);
		assertFalse(result);
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void cPRNumberEqualsOtherObject()
	{
		@SuppressWarnings("unlikely-arg-type")
		boolean result = number.equals(new ArrayList<UserID>());
		assertFalse(result);
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void cPRNumberEqualsTokenIDWithNull()
	{
		UserID number2 = new UserIDImplementation(null);
		boolean result = number.equals(number2);
		assertFalse(result);
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void cPRNumberEqualsCPRNumberIDBOTHNull()
	{
		UserID num1 = new UserIDImplementation(null);
		UserID num2 = new UserIDImplementation(null);
		boolean result = num1.equals(num2);
		assertTrue(result);
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void cPRNumberEqualsCPRnumberWithDifferentInits()
	{
		UserID number2 = new UserIDImplementation("000000-0000");
		boolean result = number.equals(number2);
		assertFalse(result);
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void cPRNumbeEqualscPRNumbe()
	{
		UserID number2 = new UserIDImplementation(id);
		boolean result = number.equals(number2);
		assertTrue(result);
	}

}
