package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.URI;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import token.TokenID;
import token.TokenRepresentationImplementation;

/**
 * Made to testing representation object.
 * @author s164166 Patrick
 */
public class TokenRepresentationImplementationTest
{
	URI path;
	
	TokenRepresentationImplementation tokenRepresentation;
	
	@Rule public MockitoRule mockitoRule = MockitoJUnit.rule();
	
	/**
	 * @author s164166 Patrick
	 */
	@Before
	public void init()
	{
		path = URI.create("www.google.com");
		tokenRepresentation = new TokenRepresentationImplementation("tempUser", path);

	}
	 
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void createTokenImplementation()
	{
		String tempID = "tempID";
		tokenRepresentation = new TokenRepresentationImplementation(tempID, path);
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void getPath()
	{
		URI returned = tokenRepresentation.getPath();
		assertEquals(returned, path);
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void getTokenAndTokenRepresentationCompareHashcode()
	{
		TokenRepresentationImplementation tokenRep2 = new TokenRepresentationImplementation("tempUser", path);
		boolean result = (tokenRepresentation.getIDhashcode() == tokenRep2.getIDhashcode());
		assertTrue(result);
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void getTokenAndTokenRepresentationCompareHashcodeFalse()
	{
		TokenRepresentationImplementation tokenRep2 = new TokenRepresentationImplementation("Not the same", path);
		boolean result = (tokenRepresentation.getIDhashcode() == tokenRep2.getIDhashcode());
		assertFalse(result);
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void getTokenAndTokenRepresentationEquality()
	{
		TokenRepresentationImplementation tokenRep2 = new TokenRepresentationImplementation("tempUser", path);
		boolean result = (tokenRepresentation.equals(tokenRep2.getTokenID()));
		assertTrue(result);
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void getTokenAndTokenRepresentationEqualityNotSame()
	{
		TokenRepresentationImplementation tokenRep2 = new TokenRepresentationImplementation("Not the Same", path);
		boolean result = (tokenRepresentation.equals(tokenRep2.getTokenID()));
		assertFalse(result);
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void getTokenID()
	{
		TokenID token = tokenRepresentation.getTokenID();
		assertNotNull(token);
	}
}
