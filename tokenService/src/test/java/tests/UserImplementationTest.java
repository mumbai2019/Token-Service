package tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import customer.User;
import customer.UserID;
import customer.UserIDImplementation;
import customer.UserImplementation;

/**
 * Made to testing TokenIDImplementation.
 * @author s164166 Patrick
 */
public class UserImplementationTest
{
//	String name = "Patrick";
	UserID userID;
	User customer;
	@Rule public MockitoRule mockitoRule = MockitoJUnit.rule();
	
	/**
	 * @author s164166 Patrick
	 */
	@Before
	public void init()
	{
		userID =  new UserIDImplementation("229709-1389");
		customer = new UserImplementation(userID);
	}	

	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void getUserID()
	{
		UserID result = customer.getUserID();
		assertEquals(userID, result);
	}
	

}
