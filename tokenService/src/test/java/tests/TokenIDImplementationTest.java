package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import token.TokenID;
import token.TokenIDImplementation;

/**
 * Made to testing TokenIDImplementation.
 * @author s164166 Patrick
 */
public class TokenIDImplementationTest
{
	TokenIDImplementation tokenID;
	String id = "tempID";
	@Rule public MockitoRule mockitoRule = MockitoJUnit.rule();
	
	/**
	 * @author s164166 Patrick
	 */
	@Before
	public void init()
	{
		tokenID = new TokenIDImplementation("tempID");
	}
	 
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void createTokenIDImplementation()
	{
		new TokenIDImplementation("tempID");
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void TokenIDGetHashcodeIDisNull()
	{
		int result = new TokenIDImplementation(null).hashCode();
		assertEquals(31, result);
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void tokenIDgetHashcode()
	{
		String input = "temp";
		int expected = 31+ input.hashCode();
		int result = new TokenIDImplementation(input).hashCode();
		assertEquals(expected, result);
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void tokenIDEqualsSameObject()
	{
		boolean result = tokenID.equals(tokenID);
		assertTrue(result);
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void tokenIDEqualsNull()
	{
		boolean result = tokenID.equals(null);
		assertFalse(result);
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void tokenIDEqualsOtherObject()
	{
		@SuppressWarnings("unlikely-arg-type")
		boolean result = tokenID.equals(new ArrayList<TokenID>());
		assertFalse(result);
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void tokenIDEqualsTokenIDWithNull()
	{
		TokenID tokenID2 = new TokenIDImplementation("not null");
		TokenID tokenID = new TokenIDImplementation(null);
		boolean result = tokenID.equals(tokenID2);
		assertFalse(result);
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void tokenIDEqualsTokenIDBOTHNull()
	{
		TokenID tokenID2 = new TokenIDImplementation(null);
		TokenID tokenID = new TokenIDImplementation(null);
		boolean result = tokenID.equals(tokenID2);
		assertTrue(result);
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void tokenIDEqualsTokenIDWithDifferentInits()
	{
		TokenID tokenID2 = new TokenIDImplementation("Not TokenID");
		boolean result = tokenID.equals(tokenID2);
		assertFalse(result);
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void tokenIDEqualsTokenID()
	{
		TokenID tokenID2 = new TokenIDImplementation("tempID");
		boolean result = tokenID.equals(tokenID2);
		assertTrue(result);
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void getTokenID()
	{
		boolean result = tokenID.getID().equals(id);
		assertTrue(result);
	}
	

}
