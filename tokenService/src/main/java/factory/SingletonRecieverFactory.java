package factory;

import rabbitmq.ConsumerImplementation;
import rabbitmq.ConsumerInterface;
import rabbitmq.Receiver;

/**
 * A singleton reciever factory
 * Used to get a reciever object used for rest and rabbitmq purposes.
 * @author s164166 Patrick
 */
public class SingletonRecieverFactory {
	private static Receiver instance;
	private static String[] keys = {"token"};
	
	/**
	 * A singleton instance spawner of the reciever
	 * @return
	 * @author s164166 Patrick
	 */
	public static synchronized Receiver getReciever()
	{
		if (instance == null)
		{
			ConsumerInterface consumer = new ConsumerImplementation();
			instance = new Receiver(keys, consumer);
		}
		return instance;
		
	}
}
