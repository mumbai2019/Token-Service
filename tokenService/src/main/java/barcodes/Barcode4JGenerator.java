package barcodes;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;

import org.krysalis.barcode4j.HumanReadablePlacement;
import org.krysalis.barcode4j.impl.code39.Code39Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.krysalis.barcode4j.tools.UnitConv;

/**
 * Barcode generator usingBarcode4j
 * @author Diego
 */
public class Barcode4JGenerator implements Barcode
{
	private Code39Bean bean = new Code39Bean();
	private final int dpi = 150;
	
	/**
	 * Barcode generator usingBarcode4j
	 * @author Diego
	 */
	@Override
	public URI generateBarcode(String token, String userName) throws IOException
	{
		String location;
		bean.setModuleWidth(UnitConv.in2mm(1f /dpi));
		bean.setWideFactor(3);
		bean.doQuietZone(false);
		bean.setMsgPosition(HumanReadablePlacement.HRP_NONE);
		File tmpDir = new File("resources");
		if (!tmpDir.exists())
		{
			tmpDir.mkdir();
		}
		tmpDir = new File(System.getProperty("user.dir") + "/resources/barcodes");
		if (!tmpDir.exists())
		{
			tmpDir.mkdir();
		}
		location = "http://02267-mumbai.compute.dtu.dk:4001/"+ token;
		
		File outputFile = new File(System.getProperty("user.dir") + "/resources/barcodes/"+token+".png");
        OutputStream out = new FileOutputStream(outputFile);
        try 
        {
            //Set up the canvas provider for monochrome PNG output
            BitmapCanvasProvider canvas = new BitmapCanvasProvider(out, "image/x-png", dpi, BufferedImage.TYPE_BYTE_BINARY, false, 0);
            //Generate the barcode
            bean.generateBarcode(canvas, token);
            //Signal end of generation
            canvas.finish();
        } finally {
            out.close();
        }
       
        URI uri = null;
        try {
        	uri = new URI(location);			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
        return uri;
	}
}
