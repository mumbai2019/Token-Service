package token;

public interface Token
{
	public Object getLocation();
	public boolean getTokenStatus();
	public void useToken();
	public TokenID getID();
}
