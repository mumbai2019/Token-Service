package token;

import java.io.IOException;
import java.net.URI;

import barcodes.Barcode4JGenerator;

public class TokenImplementation implements Token
{
	private boolean status = false;
	private TokenID id;
	private URI location;

	/**
	 * Generates a Token with a ID that should be shared between it and its TokenRepresentation
	 * @param id
	 * @param userID
	 * @author s164166 Patrick
	 */
	public TokenImplementation(String id, String userID) 
	{
		this.id = new TokenIDImplementation(id);
		// TOKEN barcode URI
		try {
			this.location = new Barcode4JGenerator().generateBarcode(id, userID);
		}catch(IOException ex){
			System.out.println(ex.getMessage());
		}			
	}
	
	/**
	 * Returns the location of a barcode
	 * @return location of barcode
	 * @author s164166 Patrick
	 */
	@Override
	public URI getLocation() {
		return location;
	}
	
	/**
	 * Returns whether the token was used or not. If the token has been used it returns true otherwise false.
	 * @return token use status.
	 * @author s164166 Patrick 
	 */
	@Override
	public boolean getTokenStatus() {
		return status;
	}

	/**
	 * Uses the token
	 * @author s164166 Patrick
	 */
	@Override
	public void useToken() {
		status = true;		
	}


	/**
	 * @return ID of the token
	 * @author s164166 Patrick
	 */
	@Override
	public TokenID getID()
	{
		return id;
	}
	
}
