package database;

import java.util.Optional;

import token.Token;
import token.TokenID;

public interface TokenDBOld 
{
	public void addToken(Token token);
	public boolean tokenExists(TokenID tokenID);
	public Optional<Token> getToken(TokenID tokenID);
}
