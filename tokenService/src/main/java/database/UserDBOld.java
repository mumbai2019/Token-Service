package database;

import java.util.Optional;

import customer.UserID;
import customer.User;
import token.TokenID;

public interface UserDBOld
{
	public void addUser(User customer);
	public boolean userExists(UserID user);
	public Optional<User> getTokenOwner(TokenID tokenID);
	public Optional<User> getCustomer(UserID cpr);
}
