package database;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import customer.User;
import token.TokenID;

/**
 * A customer DB that handles the users. This implementation uses an arrayList as it DB type
 * @author s164166 Patrick
 */
public class UserDB implements Database
{
	private List<User> customerDB = new ArrayList<User>();

	/**
	 * Adds a customer to the database
	 * @param element - the customer
	 * @author s164166 Patrick
	 */

	@Override
	public void addElement(Object element) {
		customerDB.add((User)element);		
	}

	/**
	 * Checks whether the customer objects exists in the database
	 * @param elementID - customer object
	 * @return customer object
	 * @author s164166 Patrick
	 */

	@Override
	public boolean elementExists(Object elementID) {
		return 	customerDB.parallelStream().filter(person -> person.getUserID().equals(elementID)).findAny().isPresent();
	}

	/**
	 * Returns an optional customer object if the CPR number exists.
	 * @param elementID the ID number of the customer you are looking for
	 * @return Optional of existing customer
	 * @author s164166 Patrick
	 */
	@Override
	public Object getElement(Object elementID) {
		return customerDB.parallelStream().filter(person -> person.getUserID().equals(elementID)).findFirst();
	}
	
	/**
	 * Gets the tokenOwner as an optinal
	 * @param tokenID the ID of the token
	 * @return Optional with the token owner.
	 * @author s164166 Patrick
	 */

	public Optional<User> getTokenOwner(TokenID tokenID){
		return customerDB.parallelStream().filter(person -> person.hasToken(tokenID)).findFirst();
	}
}
