package database;


/**
 * Interface for database
 * @author Diego
 *
 */
public interface Database {
	
	public void addElement(Object element);
	public boolean elementExists(Object elementID);
	public Object getElement(Object elementID);

}
