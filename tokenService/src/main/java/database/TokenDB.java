package database;

import java.util.ArrayList;
import java.util.List;
import token.Token;

/**
 * TokenDatabase Implemnetation
 * @author Arada
 *
 */
public class TokenDB implements Database
{
	private List<Token> tokenList = new ArrayList<Token>();

	/**
	 * Adds a token
	 * @author s164166 Patrick
	 */
	@Override
	public void addElement(Object element)
	{
		tokenList.add((Token)element);
		
	}

	/**
	 * Verifies whether a token exists in the database.
	 * @author s164166 Patrick
	 */
	@Override
	public boolean elementExists(Object elementID) 
	{
		return tokenList.parallelStream().filter(token -> token.getID().equals(elementID)).count() >= 1;
	}

	/**
	 * Gets a token object.
	 * @author s164166 Patrick
	 */
	@Override
	public Object getElement(Object elementID) {
		
		return tokenList.parallelStream().filter(token -> token.getID().equals(elementID)).findFirst();
	}
}
