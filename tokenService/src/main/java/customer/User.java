package customer;

import java.util.List;

import token.TokenID;

public interface User
{
	public UserID getUserID();
	public void addTokenID(TokenID tokenID);
	public boolean hasToken(TokenID tokenID);
	public List<TokenID> getTokenIDs();
}
