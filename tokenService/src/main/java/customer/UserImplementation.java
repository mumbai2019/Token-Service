package customer;

import java.util.ArrayList;
import java.util.List;

import token.TokenID;

/**
 * Generic implementation of a user and its tokens.
 * @author Arada
 *
 */
public class UserImplementation implements User
{
	private UserID userID;
	private List<TokenID> tokenIDs = new ArrayList<TokenID>();
	
	/**
	 * Creates a user with a name and a CPR number which is transforme into a value object
	 * @param name
	 * @param userID
	 */
	public UserImplementation(UserID userID) {
		this.userID = userID;
	}

	/**
	 * @return Returns the CPR number value object
	 * @author s164166 Patrick
	 */
	@Override
	public UserID getUserID() {
		return userID;
	}

	/**
	 * Adds a tokenID to the list of tokens the users owners
	 *  @author s164166 Patrick
	 */
	@Override
	public void addTokenID(TokenID tokenID)
	{
		tokenIDs.add(tokenID);
	}

	/**
	 * Confirms whether a token belongs to a person
	 *  @author s164166 Patrick
	 */
	@Override
	public boolean hasToken(TokenID tokenID)
	{
		return tokenIDs.contains(tokenID);		
	}
	

	/**
	 * Returns the tokenIDs owned by a person
	 *  @author s164166 Patrick
	 */
	@Override
	public List<TokenID> getTokenIDs() {
		return tokenIDs;
	}
	
	
	
}
