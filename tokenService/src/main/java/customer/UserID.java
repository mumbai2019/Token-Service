package customer;

/**
 * 
 * @author s172596 Diego
 *
 */
public interface UserID {

	public String getID();
}
