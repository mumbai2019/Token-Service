package main;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.concurrent.TimeoutException;

import org.json.JSONException;
import org.json.JSONObject;
import logic.TokenHandling;
import rabbitmq.EventEnums;
import rabbitmq.Transmitter;

/**
 * Handles the events running through it. 
 * @author s164166 Patrick
 */
public class EventHandling extends Thread {

	List<JSONObject> events;
	TokenHandling handler;
	Transmitter transmitter;
	
	/**
	 * An event handler object, takes a list of events a handler and a transmitter Object
	 * @param events
	 * @param handler
	 * @param transmitter
	 * @author s164166 Patrick
	 */
	public EventHandling(List<JSONObject> events, TokenHandling handler, Transmitter transmitter)
	{
		this.handler = handler;
		this.events = events;
		this.transmitter = transmitter;
	}
	
	/**
	 * Starts the thread and makes it run.
	 * @author s164166 Patrick
	 */
	@Override
	public void run()
	{
		while(true)
		{
			try {
				handle();
			} catch (JSONException | IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Constantly handles an event and performs actions on it
	 * @throws UnsupportedEncodingException
	 * @throws JSONException
	 * @throws IOException
	 */
	public void handle() throws UnsupportedEncodingException, JSONException, IOException
	{
		if (!events.isEmpty())
		{
			JSONObject json = events.remove(0);
			EventEnums enumEvent = EventEnums.valueOf(json.getString("event"));
			try {
				enumEvent.handle(handler, json, transmitter);
			} catch (TimeoutException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
		}
	}
}
