package rabbitmq;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import logic.TokenHandling;

import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;


/**
 * @author s160902
 * Receives messages from RabbitMQ and handles them asynchronously.
 */
public class Receiver extends Thread {
	private final static String EXCHANGE_NAME = "serviceExchange";
	private final String host = "rabbitmq";
	private String[] routingKeys;
	private ConsumerInterface consume;
	
	public Receiver(String[] keys, ConsumerInterface consumer)
	{
		routingKeys = keys;
		consume = consumer;
	}
	
	public void run() {
		try {
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost(host);
			System.out.println("setup host");
			Connection connection = factory.newConnection();
			Channel channel = connection.createChannel();
			System.out.println("connection");
			channel.exchangeDeclare(EXCHANGE_NAME, "direct");
			String queueName = channel.queueDeclare().getQueue();
			
			for(String routingKey : routingKeys) {
				channel.queueBind(queueName, EXCHANGE_NAME, routingKey);
				System.out.println("route: " + routingKey);
			}
			
			DeliverCallback deliverCallback = (consumerTag, delivery) -> {
				consume.consumeMsg(consumerTag, new String(delivery.getBody(), "UTF-8"), Transmitter.getInstance(), TokenHandling.getInstance());
		    };
			
		    channel.basicConsume(queueName, true, deliverCallback, consumerTag -> { });
		    
		} catch (IOException | TimeoutException e) {
			e.printStackTrace();
		}
	}
}
