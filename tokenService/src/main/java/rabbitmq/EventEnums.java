package rabbitmq;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Optional;
import java.util.concurrent.TimeoutException;

import org.json.JSONException;
import org.json.JSONObject;

import customer.UserID;
import logic.TokenHandling;
import token.TokenID;
import token.TokenIDImplementation;

/**
 * An event enum list. Given that switch cases were not preferable to enums we made a series of enum objects
 * These objects can then be treated like a switchcase with a generic handle function inserted in each.
 * <br>
 * List of events:<br>
 * <ul>
 * <li> TOKENCPREVENT, given a tokenID, use the token if possible and return the CPR number. An empty string is sent if no CPR number exists. Sends a TOKENAUTHCPREVENT back
 * <li> TOKENAUTHCPREVENT, a tokenCPREVENT contains the CPR number of the person.
 * </ul>
 * @author s164166 Patrick
 *
 */
public enum EventEnums 
{
	TOKENCPREVENT
	{
		/**
		 * gets a tokenID, sees if it exists. validates it, uses it and then if it was used returns the CPR number
		 * @author s164166 Patrick
		 */
		@Override
		public void handle(TokenHandling handle, JSONObject json, Transmitter transmitter) throws UnsupportedEncodingException, JSONException, IOException, TimeoutException
		{
			TokenID tokenID = new TokenIDImplementation(json.getString("token"));
			boolean result = false;
			String userID = "";
			if (handle.validateToken(tokenID))
			{
				Optional<UserID> optCPR = handle.getUserID(tokenID);
				if (optCPR.isPresent())
				{
					result = handle.useToken(tokenID);
					if (result)
					{
						userID = optCPR.get().getID();
					}
				}
			}
			JSONObject outgoingJson = new JSONObject();
			outgoingJson.put("event", TOKENAUTHCPREVENT);
			outgoingJson.put("token", tokenID.getID());
			outgoingJson.put("result", result);
			outgoingJson.put("cpr", userID);
			transmitter.pushMessageTo(outgoingJson.toString(), "payout");
		}
	},
	TESTEVENT{
		@Override
		public void handle(TokenHandling handle, JSONObject json, Transmitter transmitter)
				throws UnsupportedEncodingException, JSONException, IOException, TimeoutException {
				transmitter.pushMessageTo(json.toString(), "payout");
				System.out.println("WE ARE RETURNING A CALL");
			
		}
	},
	TOKENAUTHCPREVENT{
		@Override
		public void handle(TokenHandling handle, JSONObject json, Transmitter transmitter)
				throws UnsupportedEncodingException, JSONException, IOException {
			
		}};
	

	
	public abstract void handle(TokenHandling handle, JSONObject json, Transmitter transmitter) throws UnsupportedEncodingException, JSONException, IOException, TimeoutException;
}
