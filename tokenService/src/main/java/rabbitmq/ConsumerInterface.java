package rabbitmq;

import logic.TokenHandling;

public interface ConsumerInterface {
	public void consumeMsg(String tag, String msg, Transmitter transmitter, TokenHandling handler);
}
