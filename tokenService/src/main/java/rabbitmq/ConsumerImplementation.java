package rabbitmq;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.TimeoutException;

import org.json.JSONException;
import org.json.JSONObject;

import logic.TokenHandling;

public class ConsumerImplementation implements ConsumerInterface{

	
	public ConsumerImplementation(){
	}
	
	
	@Override
	public void consumeMsg(String tag, String msg, Transmitter transmitter, TokenHandling handler) 
	{
		System.out.println("CONSUMER GOT: " + tag+ " : " + msg);

		JSONObject jsonInput = new JSONObject(msg);
		EventEnums enumEvent = EventEnums.valueOf(jsonInput.getString("event"));
		try {
			enumEvent.handle(handler, jsonInput, transmitter);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
