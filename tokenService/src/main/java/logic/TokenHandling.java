package logic;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import customer.UserID;
import customer.UserIDImplementation;
import customer.User;
import customer.UserImplementation;
import database.UserDBOld;
import database.UserDB;
import database.TokenDBOld;
import database.TokenDB;
import token.Token;
import token.TokenID;
import token.TokenIDImplementation;
import token.TokenImplementation;

/**
 * A class made for handling business logic for the tokens.
 * @author s164166 Patrick, s172596 Diego
 */
public class TokenHandling 
{
	private TokenDB tokenDB = new TokenDB();
	private UserDB customerDB = new UserDB();
	private static TokenHandling instance;
	
	/**
	 * Generates a token handling object.
	 * @author s164166 Patrick, s172596 Diego
	 */
	private TokenHandling()
	{
	}
	
	/**
	 * Singleton creator
	 * @return
	 * @author s164166
	 */
	public static TokenHandling getInstance() {
		if( instance == null ) {
			instance = new TokenHandling();
		}
		return instance;
	}
	
	/**
	 * Returns whether a token ID exists as an optional
	 * @param tokenID ID of the token
	 * @return optional of token
	 * @author s164166 Patrick
	 */
	public Optional<Token> getToken(String tokenID)
	{
		return (Optional<Token>) tokenDB.getElement(new TokenIDImplementation(tokenID));
	}
	
	/**
	 * verifies whether a token exists or not
	 * @param tokenID
	 * @return if it returns true or not.
	 * @author s164166 Patrick
	 */
	public boolean validateToken(TokenID tokenID)
	{
		return tokenDB.elementExists(tokenID);
	}
	
	/**
	 * Takes a tokenID and use the corresponding token if possible
	 * @param tokenID
	 * @return whether it was used or not
	 * @author s164166 Patrick
	 */
	public boolean useToken(TokenID tokenID)
	{
		Optional<Token> optToken = (Optional<Token>)tokenDB.getElement(tokenID);
		if (optToken.isPresent())
		{
			Token token = optToken.get();
			if (!token.getTokenStatus())
			{
				token.useToken();
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Gets the CPR number of the person owning the token.
	 * @param tokenID
	 * @return optional with cpr number
	 * @author s172596 Diego
	 */
	public Optional<UserID> getUserID(TokenID tokenID)
	{
		Optional<User> customer = customerDB.getTokenOwner(tokenID);
		Optional<UserID> userID = Optional.empty();
		if (customer.isPresent())
			userID = Optional.of(customer.get().getUserID());
		return userID;
	}
	
	/**
	 * get the amount of tokens that are unused
	 * @param userID
	 * @return count of >= 0
	 * @author s164166 Patrick
	 */
	public long getUnusedTokenAmm(UserID userID)
	{
		Optional<User> customer = (Optional<User>)customerDB.getElement(userID);
		if( !customer.isPresent() )
			return 0;
		return customer.get().getTokenIDs().parallelStream().filter(
				tokenID -> ((Optional<Token>)tokenDB.getElement(tokenID)).isPresent() &&
				!((Optional<Token>)tokenDB.getElement(tokenID)).get().getTokenStatus())
				.count();
	}
	
	/**
	 * Given a customer object add 1-5 tokens to the person if they have 0 or 1 unused tokens.
	 * @param customer - who is having tokens added
	 * @param tokenReqAmm - a number between 1 and 5.
	 * @return list of TokenIDs
	 * @author s164166 Patrick
	 */
	public List<TokenID> addTokens(UserID userID, int tokenReqAmm)
	{
		if (!userExists(userID))
		{
			addToDatabase(userID);
		}
		List<TokenID> tokens = new ArrayList<TokenID>();
		if (tokenReqAmm > 0 && tokenReqAmm < 6)
		{
			if (getUnusedTokenAmm(userID) <= 1)
			{
				for(int i = 0; i < tokenReqAmm; i++)
				{
					tokens.add(generateToken(userID));	
				}
			}
		}
		return tokens;
	}
	
	/**
	 * Generates a token for the customer and adds it to the database. Then returns the TokenID of the token/
	 * @param userID
	 * @return tokenID
	 * @author s164166 Patrick
	 */
	private TokenID generateToken(UserID userID)
	{
		Optional<User> customer = (Optional<User>)customerDB.getElement(userID);
		if( !customer.isPresent() )
			return null;
		
		String id =  UUID.randomUUID().toString();
		TokenID tokenID = new TokenIDImplementation(id);
		Token token = new TokenImplementation(id, userID.getID());
		tokenDB.addElement(token);
		
		customer.get().addTokenID(tokenID);
		return tokenID;
	}
	
	
	/**
	 * Adds a customer to the database
	 * @param userID
	 * @author s164166 Patrick
	 */
	public void addToDatabase(UserID userID)
	{
		if ( !userExists(userID) )
			customerDB.addElement(new UserImplementation(userID));
	}
	
	/**
	 * Checks if a customer exists in the DB
	 * @param customer
	 * @return whether customer exists or dosn't
	 * @author s164166 Patrick
	 */
	public boolean userExists(UserID userID)
	{
		return customerDB.elementExists(userID);
	}
	
	
	/**
	 * Returns the token from the tokenID.
	 * @param tokenID
	 * @return
	 * @author s172596
	 */
	public TokenImplementation getToken(TokenID tokenID) {
		Optional<Token> token = (Optional<Token>)tokenDB.getElement(tokenID);
		if( token.isPresent() ) {
			return (TokenImplementation)token.get();
		}
		return null;
	}
}
