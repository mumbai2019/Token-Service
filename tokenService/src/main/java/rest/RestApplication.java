package rest;

import javax.ws.rs.core.Application;

import factory.SingletonRecieverFactory;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("/") //Don't touch this
public class RestApplication extends Application {
	
	/**
	 * Generates a reciever on startup that starts listening to rabbitMQ events
	 * @author s164166 Patrick
	 */
	public RestApplication() {
		SingletonRecieverFactory.getReciever().start();

	}
}