package rest;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.json.JSONException;
import org.json.JSONObject;

import customer.UserID;
import customer.UserIDImplementation;
import logic.TokenHandling;
import token.TokenID;
import token.TokenIDImplementation;
import token.TokenImplementation;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

//import enums.EventEnums;

import java.util.List;

import javax.imageio.ImageIO;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;

/**
 * @author s164161, s172596
 * 
 * 
 */
@Path("/tokens")
public class TokenEndpoint {

	TokenHandling tokenHandling = TokenHandling.getInstance();

	@GET
	@Produces("text/plain")
	public Response doGet() {
		return Response.ok("Welcome to the token rest-interface").build();
	}

	@POST
	@Path("/dispatcher")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getTokens(String inputJsonObj) {
		JSONObject json = new JSONObject(inputJsonObj);

		try {
			UserID userID = new UserIDImplementation(json.getString("userID"));
			int tokenAmount = json.getInt("tokenAmount");

			List<TokenID> tokens = tokenHandling.addTokens(userID, tokenAmount);
			List<String> strings = new ArrayList<String>();
			for (TokenID token : tokens)
			{
				strings.add(token.getID());
			}
			json = new JSONObject();
			json.put("tokens", strings);

			return Response.status(Response.Status.OK).entity(json.toString()).type(MediaType.APPLICATION_JSON).build();

		} catch (JSONException e) {
			return Response.status(Response.Status.BAD_REQUEST).type(MediaType.APPLICATION_JSON).build();
		}
	}

	@GET
	@Path("/barcode/{tokenID}")
	@Produces("image/png")
	public Response getFullImage(@PathParam("tokenID") String tokenID) {
		try {
			TokenImplementation tokenImp = tokenHandling.getToken(new TokenIDImplementation(tokenID));
			if( tokenImp == null ) {
				return Response.status(Response.Status.FORBIDDEN).build();
			}
			
			System.out.println("Working Directory = " +
					System.getProperty("user.dir"));
			
			File file = new File(System.getProperty("user.dir") + "/resources/barcodes/"+tokenID+".png");
			if( !file.exists() )
				return Response.status(Status.NOT_ACCEPTABLE).build();
			
			System.out.println(file);
			
			BufferedImage image = ImageIO.read( file );
			ByteArrayOutputStream baos = new ByteArrayOutputStream(256);
			ImageIO.write(image, "png", baos);
			byte[] imageData = baos.toByteArray();
			
			ResponseBuilder response = Response.ok(imageData);
//			response.header("Content-Disposition", "attachment; filename=1234.png");
			return response.build();
			
		}catch(Exception e) {
			return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).type(MediaType.APPLICATION_JSON).build();
		}
	}
	
//	@POST
//	@Path("/barcodeDispatcher")
//	@Consumes("application/json")
//	@Produces("application/json")
//	public Response getBarcodes(String inputJsonObj) {
//
//		try {
//			JSONObject json = new JSONObject(inputJsonObj);
//			String tokenStr = json.getString("token");
//
//			TokenImplementation tokenImp = tokenHandling.getToken(new TokenIDImplementation(tokenStr));
//			json = new JSONObject();
//			json.put("path", "http://02267-mumbai.compute.dtu.dk:4001/" + tokenImp.getLocation().toString());
//
//			return Response.status(Response.Status.OK).entity(json.toString()).type(MediaType.APPLICATION_JSON).build();
//
//		} catch (JSONException e) {
//			return Response.status(Response.Status.BAD_REQUEST).type(MediaType.APPLICATION_JSON).build();
//		}
//	}

}